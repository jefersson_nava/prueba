package co.jefersson.nunez



import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional
import org.apache.shiro.crypto.hash.Sha256Hash
import org.springframework.dao.DataIntegrityViolationException
import grails.util.GrailsUtil

class UserController {
	def FileUploadService
	def searchableService
	def EXPIRATION_TIME_RECOVERY = 604800000 //una semana
	
	static allowedMethods = [save: "POST", update: "PUT", delete: ['GET', 'POST'],create: ['GET', 'POST'], edit: ['GET', 'POST']]

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		def userInstanceList = User.list(params)
		[userInstanceList:userInstanceList,userInstanceCount: User.count()]
	}

	def search(){
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def total = 0
		def list = [:]
		if (!params.query?.trim()) {
			list = User.list(params)
			total = User.count()
		}else{
			def searchResults = User.search(params.query, params)
			total = searchResults.total
			list = searchResults.results
		}
		params.remove("query")
		render template:'listUsuarios',model:[userInstanceList:list,userInstanceCount:total]
		return
	}
	
	def show() {
		def userInstance = User.get(params.id)
		if(!userInstance){
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
			redirect action: "index"
		}
		[userInstance: userInstance]
	}

	def create() {
		switch (request.method) {
			case 'GET':
				def roleInstanceList = Role.findAll(){
					name != 'Super Administrador'
				}
				render template:'form',model:[userInstance:new User(params),roleInstanceList:roleInstanceList]
			break
			case 'POST':
				def userInstance = new User(params)
				userInstance.passwordHash = new Sha256Hash(params.passwordHash).toHex()
				
				//Subir foto
				def foto = request.getFile('foto')
				if(foto){
					//Guardar imagen perfil
					params.url = subirImagen(foto)
					userInstance.urlPhoto = params.url
				}
				
				if (!userInstance.save(flush: true)) {
					flash.errors = userInstance.errors
					render 'error@'
					render template:'errors'
					return
				}
				redirect(action: "search", params: params)
				return
			break
		}
	}

	def edit() {
		switch (request.method) {
			case 'GET':
				def userInstance = User.get(params.id)
				if (!userInstance) {
					render(contentType: 'text/json'){[
							'ErrorUpdate' : 'true',
							'error' : 'No se encontró el usuario'
							]}
					return
				}
				if(userInstance.username == 'admin'){
					render(contentType: 'text/json'){[
						'ErrorUpdate' : 'true',
						'error' : 'No se permite editar el usuario'
						]}
					return
				}
				def roleInstanceList = Role.findAll(){
					name != 'Super Administrador'
				}
				render template:'form', model:[userInstance:userInstance,roleInstanceList:roleInstanceList]
			break
			case 'POST':
				def userInstance = User.get(params.id)
				if (!userInstance) {
					render 'error@'
					render 'No se encontró el usuario'
					return
				}
				if(!params.roles){
					params.roles = null
				}
				if (params.version) {
					def version = params.version.toLong()
					if (userInstance.version > version) {
						render 0
						return
					}
				}
				
				//Subir foto
				def foto = request.getFile('foto')
				if(foto){
					//Borrar imagen anterior
					def webrootDir
					if(userInstance?.urlPhoto){
						def file
						//if (GrailsUtil.environment == "production") {
							//webrootDir = '/usr/share/tomcat/webapps/img'
							//file = new File(webrootDir,userInstance?.urlPhoto.split("img")[1])
						//}else{
							webrootDir = servletContext.getRealPath("assets") //app directory
							file = new File(webrootDir,userInstance?.urlPhoto.split("assets")[1])
						//}
						file.delete()
					}
					//Guardar imagen perfil
					params.url = subirImagen(foto)
					userInstance.urlPhoto = params.url
				}
				
				userInstance.properties = params
				
				if (!userInstance.save(flush: true)) {
					flash.errors = userInstance.errors
					render 'error@'
					render template:'errors'
					return
				}
				redirect(action: "search", params: params)
				return
			break
		}
	}
	
	def delete() {
		def userInstance = User.get(params.id)
		if (!userInstance) {
			render(contentType: 'text/json'){[
					'ErrorUpdate' : 'true',
					'error' : 'No se encontró el usuario'
					]}
			return
		}
		
		if(userInstance.username == 'admin'){
			render(contentType: 'text/json'){[
				'ErrorUpdate' : 'true',
				'error' : 'No se permite eliminar el usuario'
				]}
			return
		}
	
		try {
			userInstance.delete(flush: true)
			redirect(action: "search", params: params)
			return
		}
		catch (DataIntegrityViolationException e) {
			render(contentType: 'text/json'){[
					'ErrorUpdate' : 'true',
					'error' : 'El usuario no pudo ser eliminado'
					]}
			return
		}
	}
	
	def subirImagen(imagenFile){
		FileUploadService.uploadFile(imagenFile)
	}
	
	def forgot(){
		
	}
	
	def sendMailReturnPassword(){
		println params
		String email = params.email
		if(email){
			def user = User.findByEMail(email)
			if(user){
				//Se envía el correo de recuperación
				def accountActivationToken = new ActivationToken(email: user.eMail, token: UUID.randomUUID().toString(),
					status: 0, dueDate: new Date(System.currentTimeMillis() + EXPIRATION_TIME_RECOVERY))
				String link = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/user/restorePassword?token="+accountActivationToken.token
				
				accountActivationToken.save()
				def bodyMessage = message(code: 'mail.account.restore.body.message', args:[link])
				def subjectMessage = message(code: 'mail.account.restore.subject.message')
				sendMail {
					async true
					multipart true
					to user.eMail
					subject subjectMessage
					html bodyMessage
				}
				flash.message = "Se ha enviado un correo a tu cuenta para que recuperes tu contraseña"
			} else {
				flash.message = "No existe un usuario asociado al correo electrónico"
			}
		} else {
			flash.message = "Petición inválida, es su correo?"
		}
		render view: 'forgot'
	}
	
	def restorePassword(){
		switch (request.method) {
			case 'GET':
			//recuperar el token
				if(params.token != null){
					def token = ActivationToken.findByToken(params.token)
					if(token != null && (token.status == 0 || token.status == 1)){
						if(token.dueDate.getTime()-(new Date()).getTime() >= 0){
							token.status = 1
							token.save(flush:true)
							flash.message = ""
							render view: 'restorePassword', model: [email: token.email, token: token.token]
							return
						}
						else{
							flash.message = message(code: 'restorepassword.error.duetoken.message')
						}
					} else {
						flash.message = message(code: 'restorepassword.error.invalidtoken.message')
					}
				} else {
					flash.message = message(code: 'restorepassword.error.invalidrequest.message')
				}
				render view: 'restorePassword'
				break
			case 'POST':
			println params
				def token = ActivationToken.findByToken(params.token)
				println token.status
				if(token != null && token.status == 1){
					def user = User.findByEMail(token.email)
					if(params.password != null && params.confirmPassword != null && params.password.equals(params.confirmPassword)){
						token.status = 2
						token.save(flush:true)
						user.passwordHash = new Sha256Hash(params.password).toHex()
						user.save(flush:true)
						flash.message = message(code: 'restorepassword.success.passwordchanged.message')
						render view: 'restorePassword'
						return
					} else {
						flash.message = message(code: 'restorepassword.error.passwordmissmatch.message')
						render view: 'restorePassword', model: [email: token.email, token: token.token]
					}
				} else {
					flash.message = message(code: 'restorepassword.error.invalidtoken.message')
					render view: 'restorePassword'
				}
		}
	}
	
	def userList(){
		def userInstanceList = User.list()
		render userInstanceList as JSON
	}
}
