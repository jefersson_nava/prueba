package co.jefersson.nunez

import java.util.Date;

/**
 * The Role entity.
 *
 * @author  Jefersson Nava
 * @version 1.0.0
 * @since
 *
 */
class Role {
	
	static searchable = true
	
    String name
	
	Date dateCreated
	Date lastUpdated

    static hasMany = [ users: User, permissions: String ]
    static belongsTo = User

    static constraints = {
        name(nullable: false, blank: false, unique: true)
    }
	
	static mapping = {
		autoTimestamp true
	}
}
