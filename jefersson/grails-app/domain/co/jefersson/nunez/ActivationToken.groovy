package co.jefersson.nunez

class ActivationToken {

    String email
	String token
	/*
	 * 0 - Token created and sent to the user
	 * 1 - User is updating password
	 * 2 - User is used and invalidated
	 */
	Integer status = 0
	/*
	 * 0 - Password recovery token
	 */
	Integer type = 0
	Date dueDate
	
	Date dateCreated
	Date lastUpdated
	
	static constraints = {
	}
	
	static mapping = {
		autoTimestamp true
	}
	
}
