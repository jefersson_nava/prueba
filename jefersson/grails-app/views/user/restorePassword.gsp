	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="public" />
<title>Recuperar Contraseña</title>
</head>
<body>
	<br />
	<div class="message">
		<g:link action="index" controller="user">Ir al Inicio
							</g:link>
	</div>
	<br />
	<g:if test="${flash.message}">
		<div class="message">
			${flash.message}
		</div>
	</g:if>
	<g:if test="${email != null}">
		<div class="container">
			<div class="row-fluid">
				<div class="col-md-offset-3 col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3><g:message code="user.new.password" /></h3>
						</div>
						<div class="panel-body">
							<p><g:message code="user.email.password" /></p>
							<g:form action="restorePassword" class="form-signin" role="form"
								method="post">
								<input type="hidden" name="token" value="${token}" />
								<g:field type="email" name="email" value="${email }"
									class="form-control" required="" disabled="disabled" />
								<g:passwordField name="password" class="form-control"
									required="" placeholder="Ingrese su nueva contraseña" />
								<g:passwordField name="confirmPassword" class="form-control"
									required="" placeholder="Ingrese de nuevo la contraseña" />
								<br />
								<input type="submit" value="Cambiar Contraseña"
									class="btn btn-primary" />
							</g:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</g:if>
	<g:if test="${success != null}">
		<div class="container">
			<div class="row-fluid">
				<div class="col-md-offset-3 col-md-6">
					<g:if test="${flash.message}">
						<div class="message">
							${flash.message}
						</div>
					</g:if>
				</div>
			</div>
		</div>
	</g:if>
</body>
</html>