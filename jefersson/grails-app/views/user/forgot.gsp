<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><g:message code="user.forgot.label" /></title>

    <link rel="shortcut icon" href="${assetPath(src: 'icon_profile.ico')}" type="image/x-icon">
    <asset:stylesheet src="bootstrap.min.css" />
    <asset:stylesheet src="font-awesome/css/font-awesome.css" />
    <asset:stylesheet src="animate.css" />
    <asset:stylesheet src="style.css" />

</head>

<body class="gray-bg">

    <div class="passwordBox animated fadeInDown">
    	<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold"><g:message code="user.forgot.label" /></h2>

                    <p>
                        <g:message code="user.enter.label" />
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" action="sendMailReturnPassword">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required="">
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b"><g:message code="user.send.label" /></button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Jefersson Nuñez Nava
            </div>
            <div class="col-md-6 text-right">
               <small>© 2017</small>
            </div>
        </div>
    </div>

</body>

</html>