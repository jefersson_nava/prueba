import co.jefersson.nunez.Role
import co.jefersson.nunez.User
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->
		def superAdminRole= new Role(name: 'Super Administrador')
		superAdminRole.addToPermissions('*:*')
		superAdminRole.save()
		
		def user = new User(username: "admin", passwordHash: new Sha256Hash("admin").toHex(),name:"Jefersson",eMail:"jeferssonnunez@gmail.com",
			identification:"1127075277",mobilePhone:3217222934,address:"La estancia")
		user.addToRoles(superAdminRole)
		user.save()
		println user.errors
		
    }
    def destroy = {
    }
}
