package co.jefersson.nunez

/**
 * The User entity.
 *
 * @author  Jefersson Nava
 * @version 1.0.0
 * @since
 *
 */
class User {
	
	static searchable = true
	
    String username
    String passwordHash
	
	String name
	String lastName
	String eMail
	String description
	String urlPhoto
	
	String country
	
	Long mobilePhone
	String address
	
	Integer identification
	
	Date dateCreated
	Date lastUpdated
    
    static hasMany = [ roles: Role ]

    static constraints = {
		username(nullable: false, blank: false, unique: true, size:1..120)
		passwordHash nullable:false, blank:false
		name nullable: false, blank: false, size:1..100
		lastName nullable: true, blank: true, size:0..100
		eMail nullable:false, blank:false, unique:true ,email:true
		description nullable:true, blank:true, size:0..400
		urlPhoto nullable:true, blank:true, size:0..1024
		mobilePhone nullable:true, blank:true, size:1..20
		address nullable:true, blank:true
		country nullable:true
		identification nullable: false, unique: true, blank: false, size:1..20
    }
	
	static mapping = {
		autoTimestamp true
	}
}
