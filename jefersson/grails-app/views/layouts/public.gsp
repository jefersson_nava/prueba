<%@ page import="org.apache.shiro.SecurityUtils"%>


<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><g:layoutTitle default="Grails" /></title>
    
    <asset:stylesheet src="bootstrap.min.css" />
    <asset:stylesheet src="font-awesome/css/font-awesome.min.css" />
    
    <!-- Toastr style -->
    <asset:stylesheet src="plugins/toastr/toastr.min.css" />
    
    <asset:stylesheet src="animate.css" />
    <asset:stylesheet src="style.css" />
    
    <!-- iCheck -->
    <asset:stylesheet src="plugins/iCheck/custom.css" />
    
    <!-- Chosen -->
    <asset:stylesheet src="plugins/chosen/chosen.css" />

    
     <!-- Mainly scripts -->
    <asset:javascript src="jquery-2.1.1.js" />
    <asset:javascript src="bootstrap.min.js" />
	<asset:javascript src="plugins/metisMenu/jquery.metisMenu.js" />
	<asset:javascript src="plugins/slimscroll/jquery.slimscroll.min.js" />

    <!-- Custom and plugin javascript -->
    <asset:javascript src="inspinia.js" />
    <asset:javascript src="plugins/pace/pace.min.js" />
    
    <!-- Toastr script -->
    <asset:javascript src="plugins/toastr/toastr.min.js" />
    
    <!-- iCheck -->
    <asset:javascript src="plugins/iCheck/icheck.min.js" />
    
    <!-- Chosen -->
	<asset:javascript src="plugins/chosen/chosen.jquery.js" />
	
    <asset:javascript src="application.js" />

 
    <g:layoutHead />
</head>

<body class="pace-done skin-1">

	<div id="wrapper">

		<div class="wrapper wrapper-content">
			<g:layoutBody />
		</div>


		<div class="footer">
			<div class="pull-right">Demo</div>
			<div>
				<strong>Copyright</strong> Jefersson Nava &copy; 2017
			</div>
		</div>

	</div>

</body>

</html>